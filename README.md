Quickvec
========

**Quickvec** is an expression template based vector arithmetic library running on CUDA enabled GPUs. It will create costum CUDA kernels at compile-time through using modern C++ and was designed for the use in tomographic reconstruction with [elsa](https://gitlab.lrz.de/IP/elsa).

CI Status (master)
---------------
![Pipeline status (master)](https://gitlab.lrz.de/IP/quickvec/badges/master/pipeline.svg)

Features
--------
Quickvec provides element-wise operations between vectors as well as scalars. Through utilizing expression templates, an intuitive mathematical notation is possible like
```cpp
quickvec::Vector<float> y(1024)
quickvec::Vector<float> x(1024)

... // fill x and y

VectorCUDA result = 1.2f * x + y;
```
which will be expanded to a costum CUDA kernel. This is possible for arbitrary expressions.

Benchmarks
----------
Test server:
- CPU: Intel(R) Xeon(R) CPU E5-2687W 0 @ 3.10GHz
- GPU: RTX 2080 Ti

`a`, `b` are scalars, `dc` and `dc2` is a vector of `n` elements.

| Expression | Eigen directly | CUDA directly | CUDA ET | cuBLAS |
| :--- | ---: | ---: | ----: | ----: |
| dc * dc; | 0.634 | 0.016 | 0.016 | NaN |
| dc * a - dc / dc + b * dc; | 0.631 | 0.016 | 0.016 | NaN |
| dc * a - dc / (dc + 1); | 0.656 | 0.016 | 0.016 | NaN |
| Saxpy: a * dc + dc2; | 0.646 | 0.023 | 0.024 | 0.023 |

Documentation
-------------


Requirements
------------
Quickvec relies on **C++17** in CUDA device code. This is only possible using a recent version of the clang compiler (**not** using nvcc). Current testing includes clang-8 with CUDA-10.0.

The main third party dependencies (Eigen3, Catch2) are integrated via git submodules.

Compiling
---------
Once you have cloned the git repository, compilation can be done by following these steps:

- go to the quickvec folder and create a build folder (e.g. `mkdir build; cd build`)
- run the following commands in the build folder:

```
cmake ..
make
```

You can build and run the tests by running (in the build folder):
```
make tests
```

Building against the Quickvec library
-------------------------------------
When using the Quickvec in your project, we suggest using CMake as the build system.
Set as the include directory the main folder containing `Quickvec.h`.

Contributing
------------
To get involved, please see our [contributing page.](CONTRIBUTING.md)

Contributors
------------
The **contributors** to Quickvec are:

- Jens Petit
- Tobias Lasser
