# install an Quickvec module

function(InstallQuickvecModule QUICKVEC_MODULE_NAME QUICKVEC_MODULE_TARGET_NAME QUICKVEC_MODULE_EXPORT_TARGET)
    if(QUICKVEC_INSTALL)
        # This is required so that the exported target has the name core and not quickvec_core
        set_target_properties(${QUICKVEC_MODULE_TARGET_NAME} PROPERTIES EXPORT_NAME ${QUICKVEC_MODULE_NAME})

        include(GNUInstallDirs)
        # install the module
        install(TARGETS ${QUICKVEC_MODULE_TARGET_NAME}
                EXPORT ${QUICKVEC_MODULE_EXPORT_TARGET}
                # INCLUDES DESTINATION include
                LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
                ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
                RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
        )
        # install the header files
        install(FILES ${QUICKVEC_HEADERS}
                DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/quickvec/${QUICKVEC_MODULE_NAME}
        )
        # create the config file for the module
        install(EXPORT ${QUICKVEC_MODULE_EXPORT_TARGET}
                FILE ${QUICKVEC_MODULE_EXPORT_TARGET}.cmake
                NAMESPACE Quickvec::
                DESTINATION ${INSTALL_CONFIG_DIR}
        )
    endif(QUICKVEC_INSTALL)

    # this file won't be installed,
    # it's just for linking to the elsa module in its in-repo build tree!
    # FILE has to have the same structure as the install(EXPORT DESTINATION)
    export(
        EXPORT "${QUICKVEC_MODULE_EXPORT_TARGET}"
        FILE "${CMAKE_BINARY_DIR}/quickvec/${QUICKVEC_MODULE_EXPORT_TARGET}.cmake"
        NAMESPACE quickvec::
    )
endfunction()
