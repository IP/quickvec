# Contributing to VectorCUDA
This page summarizes advice how to contribute to quickvec.

## General Workflow

#### a) Internal Developers
- Make a local clone of the repository
- Checkout a new branch and work on your new feature or bugfix
- Push your branch and create a merge request targeting master

#### b) External Contributors
- Fork the git repository
- Make a local clone your own fork
- Checkout a new branch and work on your new feature or bugfix
- Push your branch and send us a merge request targeting master

A branch should be short-lived and specific for a feature or bugfix. Commits should be squashed
into relevant groups before merging into master. Use
```
git rebase -i ${commit_to_rebase_on}
```
to clean up the commit history.

If during the development of a feature, `master` received updates, a `git rebase` should be preferred
over a `git merge` except when the changes are specifically needed in the feature branch:
```
git checkout feature-branch-name
git rebase master
```

Commit messages should follow some guidelines (derived from [this
blogpost](https://chris.beams.io/posts/git-commit/)):

- Separate subject from body with a blank line
- Limit the subject line to 50 characters
- Use the imperative mood in the subject line
- Wrap the body at 72 characters
- Use the body to explain what and why vs. how

## Testing
You can run the unit tests by running `make tests` in the build folder. To specify which tests run,
filter with `ctest -R regular_expression`.

We use a testing style described as [Behaviour-Driven
Development](https://github.com/catchorg/Catch2/blob/master/docs/tutorial.md#bdd-style) (BDD). Please
follow this style when adding new tests.

## Benchmarking
You can use the catch testing framework to do [benchmarking
](https://github.com/catchorg/Catch2/blob/master/docs/benchmarks.md).

## Style Guide
We use the tool `clang-format` to autoformat our code with the [given style
file](.clang-format). Please make sure that your code is formatted accordingly, otherwise the CI
will fail in the first stage. You can either execute
```
find quickvec/ -name '*.h' -or -name '*.hpp' -or -name '*.cpp' | xargs clang-format-8 -i -style=file $1
```
in the root folder, setup a git hook or integrate `clang-format` into your IDE. Note that we
currently use version 8.0.0, different versions might produce errors.
