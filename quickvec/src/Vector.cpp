// The following ugly workaround is necessary to compile thrust with clang
// =============================
#define THRUST_CUB_NS_PREFIX   \
    namespace thrust::cuda_cub \
    {
#define THRUST_CUB_NS_POSTFIX }
#include <thrust/system/cuda/detail/cub/util_debug.cuh>
using namespace thrust::cuda_cub::cub;
// =============================

#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/inner_product.h>

#include "Vector.h"
#include "Defines.h"

namespace quickvec
{
    template <typename data_t>
    Vector<data_t>::Vector(Eigen::Matrix<data_t, Eigen::Dynamic, 1> const& data)
        : _size(static_cast<size_t>(data.size())),
          _data(static_cast<size_t>(data.size()) * sizeof(data_t))
    {
        int device = -1;
        gpuErrchk(cudaGetDevice(&device));
        gpuErrchk(
            cudaMemcpy(_data.get(), data.data(), _size * sizeof(data_t), cudaMemcpyHostToDevice));
    };

    template <typename data_t>
    Vector<data_t>::Vector(size_t size) : _size(size), _data(size * sizeof(data_t))
    {
    }

    template <typename data_t>
    Vector<data_t>::Vector(data_t* data, size_t size, bool owning)
        : _size(size), _data(data, owning)
    {
    }

    template <typename data_t>
    Vector<data_t> Vector<data_t>::clone() const
    {
        Vector<data_t> vec(this->size());
        thrust::copy(_data.get(), _data.get() + _size, vec._data.get());
        return vec;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator=(const Vector& other)
    {
        thrust::copy(other._data.get(), other._data.get() + _size, _data.get());
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator=(Vector&& other)
    {
        _data = std::move(other._data);
        _size = std::move(other._size);
        return *this;
    }

    template <typename data_t>
    const data_t& Vector<data_t>::operator[](size_t index) const
    {
        return _data.get()[index];
    }

    template <typename data_t>
    data_t& Vector<data_t>::operator[](size_t index)
    {
        return _data.get()[index];
    }

    template <typename data_t>
    struct MulAbsOp {
        __host__ __device__ GetFloatingPointType_t<data_t> operator()(const data_t& lhs,
                                                                      const data_t& rhs)
        {
            return std::abs(lhs) * std::abs(rhs);
        }
    };

    template <typename data_t>
    GetFloatingPointType_t<data_t> Vector<data_t>::squaredL2Norm() const
    {
        if constexpr (isComplex<data_t>) {
            using Inner = GetFloatingPointType_t<data_t>;

            // thrust::inner_product doesn't use the abs value of each component as it just squares
            // it, but for complex numbers, we gotta use the abs of each value, so we gotta deal
            // with it separatly...
            return thrust::inner_product(_data.get(), _data.get() + _size, _data.get(), Inner{0},
                                         thrust::plus<Inner>{}, MulAbsOp<data_t>{});
        } else {
            return thrust::inner_product(_data.get(), _data.get() + _size, _data.get(), data_t{0});
        }
    }

    template <typename data_t>
    GetFloatingPointType_t<data_t> Vector<data_t>::l2Norm() const
    {
        return std::sqrt(squaredL2Norm());
    }

    // helper struct for L1-norm for index_t (note that generic lambdas are not working with thrust)
    template <typename T>
    struct L1Norm {
        __host__ __device__ GetFloatingPointType_t<T> operator()(T const& lhs, T const& rhs)
        {
            return std::abs(lhs) + std::abs(rhs);
        }
    };

    template <typename data_t>
    GetFloatingPointType_t<data_t> Vector<data_t>::l1Norm() const
    {
        return thrust::reduce(_data.get(), _data.get() + _size, GetFloatingPointType_t<data_t>{0},
                              L1Norm<data_t>());
    }

    // helper struct for L0-"norm" for index_t (note that generic lambdas are not working with
    // thrust)
    template <typename T>
    struct L0PseudoNorm {
        static constexpr real_t margin = 0.000001f;
        __host__ __device__ index_t operator()(const T& val) { return std::abs(val) > margin; }
    };

    template <typename data_t>
    index_t Vector<data_t>::l0PseudoNorm() const
    {
        // return _size - thrust::count(_data.get(), _data.get() + _size, 0);
        return thrust::count_if(_data.get(), _data.get() + _size, L0PseudoNorm<data_t>());
    }

    template <typename data_t>
    GetFloatingPointType_t<data_t> Vector<data_t>::lInfNorm() const
    {
        using Inner = GetFloatingPointType_t<data_t>;

        // helper struct for LInf-norm (note generic lambdas are not working with thrust)
        struct absOp {
            __host__ __device__ Inner operator()(const data_t& x) const { return std::abs(x); }
        };

        return thrust::transform_reduce(_data.get(), _data.get() + _size, absOp{}, Inner{0},
                                        thrust::maximum<Inner>{});
    }

    template <typename data_t>
    data_t Vector<data_t>::sum() const
    {
        return thrust::reduce(_data.get(), _data.get() + _size);
    }

    template <typename data_t>
    data_t Vector<data_t>::maxElement() const
    {
        if constexpr (isComplex<data_t>) {
            throw std::logic_error(
                "quickvec::Vector::maxElement: max of complex type not supported");
        } else {
            return *thrust::max_element(_data.get(), _data.get() + _size);
        }
    }

    template <typename data_t>
    data_t Vector<data_t>::minElement() const
    {
        if constexpr (isComplex<data_t>) {
            throw std::logic_error(
                "quickvec::Vector::minElement: min of complex type not supported");
        } else {
            return *thrust::min_element(_data.get(), _data.get() + _size);
        }
    }

    namespace detail
    {
        template <typename data_t>
        struct MultipliesConjLhs {
            __host__ __device__ data_t operator()(const data_t& lhs, const data_t& rhs)
            {
                return std::conj(lhs) * rhs;
            }
        };
    } // namespace detail

    template <typename data_t>
    data_t Vector<data_t>::dot(const Vector<data_t>& v) const
    {

        if constexpr (isComplex<data_t>) {
            // Eigen uses the hermitian dot product with the conjugate in the first variable.
            // So the dot product is \sum conj(v_i) * w_i for two complex vector v and w. And conj
            // the complex conjugate. See the Eigen documentation:
            // https://eigen.tuxfamily.org/dox/classEigen_1_1MatrixBase.html#adfd32bf5fcf6ee603c924dde9bf7bc39
            return thrust::inner_product(_data.get(), _data.get() + _size, v._data.get(),
                                         data_t{0.0}, thrust::plus<data_t>(),
                                         detail::MultipliesConjLhs<data_t>{});
        } else {
            return thrust::inner_product(_data.get(), _data.get() + _size, v._data.get(),
                                         data_t{0});
        }
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator+=(const Vector<data_t>& v)
    {
        eval(*this + v);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator-=(const Vector<data_t>& v)
    {
        eval(*this - v);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator*=(const Vector<data_t>& v)
    {
        eval(*this * v);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator/=(const Vector<data_t>& v)
    {
        eval(*this / v);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator+=(data_t scalar)
    {
        eval(*this + scalar);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator-=(data_t scalar)
    {
        eval(*this - scalar);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator*=(data_t scalar)
    {
        eval(*this * scalar);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator/=(data_t scalar)
    {
        eval(*this / scalar);
        return *this;
    }

    template <typename data_t>
    Vector<data_t>& Vector<data_t>::operator=(data_t scalar)
    {
        unsigned int blockSize = 256;
        auto numBlocks = static_cast<unsigned int>((_size + blockSize - 1) / blockSize);

        set<<<numBlocks, blockSize>>>(_size, scalar, _data.get());

        gpuErrchk(cudaPeekAtLastError());
        gpuErrchk(cudaDeviceSynchronize());

        return *this;
    }

    template <typename data_t>
    bool Vector<data_t>::operator==(Vector<data_t> const& other) const
    {
        return thrust::equal(_data.get(), _data.get() + _size, other._data.get());
    }

    template class Vector<float>;
    template class Vector<double>;
    template class Vector<std::complex<float>>;
    template class Vector<std::complex<double>>;
    template class Vector<index_t>;

} // namespace quickvec
